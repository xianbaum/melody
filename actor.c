#include "actor.h"

static CN_inline int GetDrawFourDirSpriteOffset(enum ObjectDir dir, int step)
{
    switch(dir)
    {
    case OBJDIR_UP:
        if(step == 1) return 7;
        return 0;
    case OBJDIR_RIGHT:
        return + (1 + step);
    case OBJDIR_DOWN:
        return + (3 + step);
    case OBJDIR_LEFT:
        return + (5 + step);
    }
}

void DrawActor(
    MotheriNESROM *rom,
    CN_Image **graphics,
    CN_Rect *camera,
    struct Actor *actor)
{
    int dirOffset = GetDrawFourDirSpriteOffset(actor->dir, actor->step);
    DrawSpriteQuad(
        graphics,
        rom,
        rom->data.rom.data.prg.interpreted.spriteQuads+actor->spriteQuadOffset
        + dirOffset,
        actor->x - camera->x,
        actor->y - camera->y);
}

void UpdateActor(struct Actor *actor, int dt)
{
    CN_bool moving;
    if(actor->x < actor->targetX) {
        actor->x++;
        moving = CN_true;
    } else if(actor->x > actor->targetX) {
        actor->x--;
        moving = CN_true;
    }
    if(actor->y < actor->targetY) {
        actor->y++;
        moving = CN_true;
    } else if(actor->y > actor->targetY) {
        actor->y--;
        moving = CN_true;
    } 
    if(actor->dir != actor->targetDir) {
        actor->dir = actor->targetDir;
    }
    if(moving) {
        actor->stepTimer++;
        if(actor->stepTimer > 9) {
            actor->step = !actor->step;
            actor->stepTimer = 0;
        }
    } else {
        actor->step = 0;
    }
}
