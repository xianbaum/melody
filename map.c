#include "cngine/cngine.h"
#include "map.h"
#include "nesrom.h"
#include "neschr.h"
#include <stdlib.h>
#include <signal.h>

#define IMPLEMENT_TILE_FUNCTION(_TYPE_,_NAME_,TILES_TOTAL, START_OFFSET,  \
                    OFFSET_INCREMENT, TILES_PER_OFFSET, TRANSFORMATION) \
static _TYPE_ *_NAME_(MotheriNESROM *rom) {                             \
    _TYPE_ *tiles = malloc(sizeof(_TYPE_)*(TILES_TOTAL));               \
    int i = 0;                                                          \
    for(; i < TILES_TOTAL; i++) {                                       \
        unsigned int rom_offset = (START_OFFSET + (((i / TILES_PER_OFFSET) * \
                                   OFFSET_INCREMENT * sizeof(_TYPE_)) + \
                 ((i % TILES_PER_OFFSET) * sizeof(_TYPE_)))), tile = 0; \
        for(; tile < sizeof(_TYPE_); tile++) {                          \
            tiles[i].data[tile] = rom->bytes[rom_offset+tile] TRANSFORMATION; \
                }                                                       \
    }                                                                   \
    return tiles;                                                       \
}


IMPLEMENT_TILE_FUNCTION(Tile16x16, CreateTile16x16FromROM, TOTAL_TILES16,
                        0x3010, 0x1000, 512, & 0b00111111)
IMPLEMENT_TILE_FUNCTION(Tile64x64, CreateTile64x64FromROM, TOTAL_TILES64,
                        0x2010, 0x400, 256, /**/)
IMPLEMENT_TILE_FUNCTION(MapTile, CreateMapTileFromROM, TOTAL_MAP_TILES,
                        0x4010, 0x4000, 8192, /**/)
IMPLEMENT_TILE_FUNCTION(TileSubPalette, CreateTileSubPaletteFromROM,
                        TOTAL_TILES64, 0x3010, 0x400, 256, >> 6)
IMPLEMENT_TILE_FUNCTION(MapSector, CreateMapSectorFromROM, 3584,
                        0x7810, 0x1000,
                        512, & 0b00111111)

/*static RGB4ColorPal **CreateMapPalettesFromROM(
    MotheriNESROM *rom, CN_RGB *nesPal)
{
    if(nesPal == NULL) return NULL;
    {RGB4ColorPal **pals = malloc(sizeof(RGB4ColorPal) * 128);
        int i = 0;
        for(; i < 128; i++) {
            pals[i] = RGB4ColorPalFromNES(
                rom->data.rom.data.prg.interpreted.mapPalettes+i, nesPal);
        }
        return pals;
    }
    }*/

MapData *CreateMapDataFromROM(MotheriNESROM *rom, CN_RGB *nesPal)
{
    MapData *mapdata = malloc(sizeof(MapData));
    mapdata->tiles16 = CreateTile16x16FromROM(rom);
    mapdata->tiles64 = CreateTile64x64FromROM(rom);
    mapdata->subpals = CreateTileSubPaletteFromROM(rom);
    mapdata->sectors = CreateMapSectorFromROM(rom);
    mapdata->map = CreateMapTileFromROM(rom);
    return mapdata;
}

void DrawTile16x16(CN_Image **graphics, Tile16x16 tile, unsigned char subpal,
                   int x, int y, int gfx_offset)
{
    int tx = 0;
    CN_Rect source, dest;
    source.x = source.y = 0;
    source.h = source.w = dest.h = dest.w = 8;
    for(;tx < TILES_PER_TILE16; tx++) {
        int ty = 0, destx, desty;
        for(; ty < TILES_PER_TILE16; ty++) {
            destx = x + (tx * 8);
            desty = y + (ty * 8);
            CN_Image_DrawOffset(
                graphics[tile.data[(ty*TILES_PER_TILE16)+tx]+gfx_offset],
                CN_GetScreen(),
                destx, desty, subpal*4);
        }
    }
}

void DrawTile16x16Page(CN_Image **graphics, Tile16x16 *tiles, int offset,
                       TileSubPalette *subpals, int gfx_offset)
{
    int x = 0;
    for(; x < 16; x++) {
        int y = 0;
        for(; y <= 12; y++) {
            DrawTile16x16(
                graphics, tiles[offset+(y*16)+x],
                0,
                ((offset+(y*16+x))/128)*64,
                y,0);
        }
    }
}

void DrawTileset(CN_Image **graphics, int gfx_offset)
{
    const int TILES_WIDTH = 16, TILES_HEIGHT = 4;
    int tx = 0, destx, desty;
    CN_Rect source, dest;
    source.x = source.y = 0;
    for(;tx < TILES_WIDTH; tx++) {
        int ty = 0;
        for(; ty < TILES_HEIGHT; ty++) {
            destx = (tx * TILE_SIZE);
            desty = (ty * TILE_SIZE);
            CN_Image_Draw(graphics[ty*TILES_WIDTH+tx+gfx_offset],
                          CN_GetScreen(),
                          destx, desty);
        }
    }
}

void DrawTile64x64(
    Tile64x64 tile, TileSubPalette subpal,
    Tileset16 tileset1, Tileset16 tileset2, int x, int y)
{
    int ty = 0;
    for(; ty < TILE16_PER_TILE64; ty++) {
        int tx = 0;
        for(; tx < TILE16_PER_TILE64; tx++) {
            unsigned char byte = tile.data[ty*TILE16_PER_TILE64+tx];
            unsigned char subpalByte = subpal.data[ty*TILE16_PER_TILE64+tx];
            unsigned char tilenum = byte & 0b01111111; /*first 7 bytes is tile*/
            unsigned char tilesetNumber = byte >> 7; /* last byte is tileset */
            Tileset16 current_tileset = tilesetNumber ? tileset2 : tileset1;
            DrawTile16x16(
                current_tileset.graphics,
                current_tileset.tiles[tilenum],
                subpalByte,
                x + (tx * TILE16_SIZE),
                y + (ty * TILE16_SIZE), 0);
        }
    }
}

Tileset16 Tileset16FromNumber(int number, CN_Image **graphics,
                              Tile16x16 *tiles16)
{
    if(!(number >= 0 && number <= 0x1f) ) number = 0;
    {Tileset16 tileset;
        tileset.graphics = graphics + (number * 64);
        tileset.tiles  = tiles16  + (number * 128);
        return tileset;
    }
}

static Tileset16 GuessSecondaryTileset16FromNumber(
   int number, CN_Image **graphics, Tile16x16 *tiles16, TileSubPalette *subpals)
{
    switch(number) {
    case 0x0a: return Tileset16FromNumber(0x09, graphics, tiles16);
    case 0x0b: return Tileset16FromNumber(0x02, graphics, tiles16);
    case 0x10: return Tileset16FromNumber(0x0f, graphics, tiles16);
    case 0x11: return Tileset16FromNumber(0x0f, graphics, tiles16);
    case 0x12: return Tileset16FromNumber(0x13, graphics, tiles16);
    case 0x13: return Tileset16FromNumber(0x12, graphics, tiles16);
    case 0x15: return Tileset16FromNumber(0x16, graphics, tiles16);
    case 0x18: return Tileset16FromNumber(0x11, graphics, tiles16);
    case 0x1b: return Tileset16FromNumber(0x06, graphics, tiles16);
    }
    return Tileset16FromNumber(number, graphics, tiles16);
}

Tile64x64 *Tileset64OffsetFromNumber(int number, Tile64x64 *tiles64)
{
    return tiles64 + number * 0x40;
}

static Tileset64 Tileset64FromNumber(int number, CN_Image **graphics,
               Tile16x16 *tiles16, Tile64x64 *tiles64, TileSubPalette *subpals)
{
    Tileset64 tileset64;
    tileset64.tileset16 = Tileset16FromNumber(number,graphics,tiles16);
    tileset64.secondary16 =
        GuessSecondaryTileset16FromNumber(number, graphics, tiles16, subpals);
    tileset64.tiles64 = Tileset64OffsetFromNumber(number, tiles64);
    return tileset64;
}
    

void DrawTile64x64Page(
    Tile64x64 *tiles64, CN_Image **graphics, Tile16x16 *tiles16, int offset,
    int tilesetnum1, int tilesetnum2, TileSubPalette *subpals)
{
    Tileset16 tileset1 =
        Tileset16FromNumber(tilesetnum1, graphics, tiles16);
    Tileset16 tileset2 =
        Tileset16FromNumber(tilesetnum2, graphics, tiles16);
    int y = 0; for(; y < 4; y++) {
        int x = 0; for(; x < 4; x++) {
            DrawTile64x64(tiles64[offset+(y*3)+x],
                          subpals[offset+(y*3)+x], tileset1, tileset2,
                          x*TILE64_SIZE, y*TILE64_SIZE);
        }
    }    
}

MapSector *MapSectorFromCoordinate(MapSector*sectors, short x, short y)
{
    return sectors +
        ((MAP_WIDTH / SECTOR_WIDTH) * (y/SECTOR_WIDTH)) +
        (x / SECTOR_WIDTH);
}

static MapTile MapTileFromCoordinate(MapTile *maptiles, short x, short y)
{
    return maptiles[(MAP_WIDTH * y) + x];
}

static Tile64x64 Tile64x64FromMapTile(MapTile maptile, Tile64x64 *tiles64)
{
    return tiles64[maptile.data[0] & 0b00111111];
}

static TileSubPalette TileSubPaletteFromMapTile(
    MapTile maptile, TileSubPalette *subpals)
{
    return subpals[maptile.data[0] & 0b00111111];
}

void DrawMap(CN_Image **graphics, MapData *mapdata, CN_Rect *camera,
             unsigned char paletteIndex)
{
    int x = camera->x/64+1,y =  camera->y/64+1,w = camera->w/64+2, h =camera->h/64+1,
        sx = x, sy = y;
    for(; y < sy+h+1; y++) {
        x = sx;
        for(; x < sx+w; x++) {
            MapSector *sector = MapSectorFromCoordinate(mapdata->sectors,x,y);
            if(paletteIndex == ANY_PALETTE_INDEX ||
               sector->model.palette == paletteIndex) {
                Tile64x64 *t64_1 = Tileset64OffsetFromNumber(
                    sector->model.tilesets[0], mapdata->tiles64);
                Tile64x64 *t64_2 = Tileset64OffsetFromNumber(
                    sector->model.tilesets[1], mapdata->tiles64);
                Tileset16 t1 = Tileset16FromNumber(
                    sector->model.tilesets[0], graphics, mapdata->tiles16);
                Tileset16 t2 = Tileset16FromNumber(
                    sector->model.tilesets[1], graphics, mapdata->tiles16);
                MapTile maptile = MapTileFromCoordinate(mapdata->map, x, y);
                Tile64x64 tile;
                TileSubPalette *s1 = Tileset64OffsetFromNumber(
                    sector->model.tilesets[0], mapdata->subpals);
                TileSubPalette *s2 = Tileset64OffsetFromNumber(
                    sector->model.tilesets[1], mapdata->subpals);
                TileSubPalette subpal;
                if(maptile.data[0] & 0b01000000) {
                    tile = Tile64x64FromMapTile(maptile, t64_2);
                    subpal = TileSubPaletteFromMapTile(maptile, s2);
                } else {
                    Tileset16 temp = t2;
                    tile = Tile64x64FromMapTile(maptile, t64_1);
                    subpal = TileSubPaletteFromMapTile(maptile, s1);
                    t2 = t1;
                    t1 = temp;
                }
                DrawTile64x64(tile, subpal, t2, t1,
                    -(camera->x % TILE64_SIZE) + ((x - sx) * TILE64_SIZE),
                    -(camera->y % TILE64_SIZE) + ((y - sy) * TILE64_SIZE));
            }
        }
    }
}

struct MapPaletteAreaLocation {
    unsigned char palIndex1;
    unsigned char palIndex2;
};

char PrepareAreaBySectorCoordinate(MapSector *sectors, NES4ColorPal *pals, 
    CN_RGB *nespalrgb, CN_RGB *pal,int x, int y)
{
    MapSector *sector = MapSectorFromCoordinate(sectors, x, y);
    PreparePalette(sector->model.palette, pals, nespalrgb, pal, 0, 3, 0);
}

void SearchForMapArea(MapData *mapdata, unsigned char area, int *outx, int *outy)
{
    int x, y;
    for(x = 0; x < MAP_WIDTH; x++) {
        for(y = 0; y < MAP_HEIGHT; y++) {
            MapSector *sector = MapSectorFromCoordinate(mapdata->sectors,x,y);
            if(sector->model.palette == area) {
                *outx = x;
                *outy = y;
                return;
            }
        }
    }
    *outx = -1;
    *outy = -1;
}
