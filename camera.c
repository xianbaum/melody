#include "camera.h"

void UpdateCamera(CN_Rect *camera, CN_Rect *map, struct Actor *actor)
{
    camera->x = actor->x - (camera->w/2);
    camera->y = actor->y - (camera->h/2);
    if(camera->x < map->x) camera->x = map->x;
    else if(camera->x + camera->w > map->w) camera->x = map->w - camera->w;
    if(camera->y < map->y) camera->y = map->y;
    else if(camera->y + camera->h > map->h) camera->y = map->h - camera->h;
}
