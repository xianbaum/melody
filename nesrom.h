#ifndef NESROM_H_
#define NESROM_H_

struct PPUTile {
    unsigned char data[16];
};

typedef unsigned char NESColor;

typedef struct NES4ColorPal {
    NESColor colors[4];
} NES4ColorPal;

typedef struct TileData{
    unsigned char data[4];
} TileData;

typedef struct Tile64x64 {
    unsigned char data[16];
} Tile64x64;

typedef struct TileByteData { /* beep */
    unsigned char data[1];
} TileByteData;

typedef union {
    unsigned char data[4];
    struct {
    unsigned char palette;
    unsigned char area;
    unsigned char tilesets[2];
    } model;
} MapSector;

typedef TileData Tile16x16;
typedef TileByteData MapTile;
typedef Tile64x64 TileSubPalette;

typedef struct MapBank {
    MapTile maptiles[0x2000];
    Tile64x64 tiles64[0x1000/16];
    union {
        struct {
            Tile16x16 tiles16[0x800/4];
            MapSector sectors[2048/4];
        } tiles;
        TileSubPalette tiles64palette[0x1000/16];
    };
} MapBank;

typedef struct SpriteData{
    char x;
    char y;
    char palAndRot;
    char tileNum;
} SpriteData;

typedef struct SpriteQuadInfo {
    unsigned char pointerStart;
    unsigned char pointerEnd;
    unsigned char additionalOffset;
    unsigned char fourPals;
} SpriteQuadInfo;

typedef struct OverworldSpriteQuad {
    SpriteData topleft;
    SpriteData topright;
    SpriteData bottomleft;
    SpriteData bottomright;
} OverworldSpriteQuad;

union MotherNESROM{
    char bytes[393216];
    struct {
    union {
        char bytes[16384*16];
        struct {
            union {
                struct {
                    char idk[0x589];                /*0x0000 thru 0x0589 */
                    /*Probably 1n28 but idk where it starts.*/
                    NES4ColorPal battlepalettes[124]; /*0x059A thru 0x078A */
                } nonmapdata;
                MapBank mapdata[8];
            } mapbanks;
            unsigned char objects[0x5de6];
            char unknown1[0x21a];
            char unknown2[0x8db];
            char townpalettes[0x2d];
            char padding1[1];
            char unknown3[0x16f6];
            char padding2[1];
            char maptileproperties[0x1000];
            NES4ColorPal mapPalettes[128];
            char randomEncounterGroups[0x200];
            char unknown4[0x18b];
            char randomEncounterRates[0x8];
            char mapAreaRandomEncounterTable[0x40];
            char unknown5[0x430];
            /*0x29a13*/
            char uknownCharacterPalettes[0x1a];
            NES4ColorPal characterPalettes[4];
            char unknown6[0x497];
            char openingCreditsAndTitleScreen1[0x50];
            /*0x29f24*/
            NES4ColorPal somePalettes[8];
            char openingCreditsAndTitleScreen2[204];
            /*0x2a010*/
            SpriteQuadInfo spriteQuads[727];
            /*0x2ab6c*/
            SpriteData spriteData[1477];
            /*0x2c010*/
        } interpreted;
    } prg;
    /*8000 - 8192 are text*/
    struct PPUTile chr[8192];
    } data;
};

union MotheriNESROM {
  unsigned char bytes[393232];
  struct {
      char header[16];
      union MotherNESROM rom;
  } data;
};

typedef union MotheriNESROM MotheriNESROM;
typedef struct PPUTile PPUTile;

extern const int MOTHER_INES_ROM_SIZE;
extern const int MOTHER_NES_ROM_SIZE;
extern const int PPU_TILE_COUNT;

MotheriNESROM *MotheriNESROM_LoadByPath(char *path);

#endif /*NESROM_H_*/
