#include "object.h"

static CN_inline unsigned char GetChar(unsigned char *data)
{
    return *data;
}

static CN_inline unsigned short GetShort(unsigned char *data)
{
    return ((*data) << 8) + (*(data + 1));
}

static CN_inline unsigned int GetInt(unsigned char *data)
{
    return ((*data << 24) + (*(data+1) << 16) + (*(data+2) << 8) + (*(data+1)));
}

static struct ParsedObject *ParseObjectHeader(
    unsigned char *data,
    MapSector * sectors,
    struct ParsedObject *obj)
{
    obj->type = GetChar(data) & 0b11111;
    obj->x = GetChar(data+1) * TILE64_SIZE - TILE16_SIZE + (GetChar(data) >> 6) * TILE16_SIZE;
    obj->dir = GetShort(data+2) & 0b11111;
    obj->y = ((GetChar(data+3) - 0x20) * TILE64_SIZE) + ((GetChar(data) >> 6) * TILE16_SIZE);
    obj->sector = MapSectorFromCoordinate(sectors, obj->x/4, obj->y/4)->data[1];
    if(obj->type > 0x08 && obj->type < 0x17) { /* Object has a sprite */
        obj->sprite = GetChar(data+4) + (GetChar(data+5) * 0x100);
        if(obj->type == OBJTYPE_INVISIBLE_CRASHES) {
/* Object has a present, not sure if correct on enum, just a guess */
            obj->presentItem = GetChar(data+6);
            obj->presentID = GetChar(data+7);
            obj->script = data+8;
        }
    }
    if(obj->type == OBJTYPE_DOOR) { /* Object is a door */
        obj->music = GetShort(data+4) & 0b11111;
        obj->doorX = GetShort(data+4) << 5;
        obj->targetDir = GetShort(data+6) & 0b11111;
        obj->doorY = GetShort(data+6) << 5;
        obj->script = data+7;
        obj->sprite = 0;
    }
    return obj;
}

static struct ParsedObject *ParseObjectPointers(
    MotheriNESROM *rom,
    unsigned char *data,
    MapSector * sectors,
    int *len,
    int bankOffset)
{
    unsigned char pointerStart, pointerEnd, *data_temp = data;
    int pointer;
    int count = 0;
   struct  ParsedObject *o;
    do {
        pointerEnd = data[(count*2)+1];
        pointerStart = data[count*2];
        pointer = pointerEnd * 0x100 + pointerStart;
        count++;
    } while(pointer != 0);
    count--;
    o = CN_malloc(sizeof(struct ParsedObject)*(count));
    *len = count;
    count = 0;
    do {
        pointerEnd = data[(count*2)+1];
        pointerStart = data[count*2];
        pointer = pointerEnd * 0x100 + pointerStart;
        if(pointer != 0) {
            ParseObjectHeader(
                rom->bytes + pointer + bankOffset,
                sectors,
                o+count);
        }
        count++;
    } while(pointer != 0);
    return o;
}

struct ParsedObject *ParseObjectsForArea(
    MotheriNESROM *rom, int area, MapSector *sectors, int *len)
{
    int bankOffset, areaOffset, objectPointerOffset;
    unsigned char *areaObjectPointerOne;
    if(area <= 0x19) {
        bankOffset = 0x18010;
        areaOffset = 0;
    } else if(area > 0x19 && area <= 0x2a) {
        bankOffset = 0x1a010;
        areaOffset = -0x1a;
    } else {
        bankOffset = 0x1c010;
        areaOffset = -0x2b;
    }
    areaObjectPointerOne =
        rom->bytes+bankOffset + 0x8000 + ((area + areaOffset) * 2);
    objectPointerOffset = areaObjectPointerOne[0] + (areaObjectPointerOne[1] * 0x100);
    return ParseObjectPointers(
        rom,
        rom->bytes + bankOffset + objectPointerOffset,
        sectors,
        len,
        bankOffset);
}

void DrawParsedObject(
    MotheriNESROM *rom,
    CN_Image **graphics,
    CN_Rect *camera,
    struct ParsedObject *object)
{
    if(object->sprite > 0x8000 && object->sprite < 0xa000) {
        DrawSpriteQuad(
            graphics + (rom->bytes[0x3d634+object->sector]*4),
            rom,
            rom->data.rom.data.prg.interpreted.spriteQuads
            + ((object->sprite - 0x8000)/4),
            object->x - camera->x,
            object->y - camera->y);
    }
}

void ExecObjectScript(char *script)
{
    char *cur = script;
    while(*cur)
    {
        switch(*cur)
        {
        case 0x00: return; /* end script */
        case 0x01: return; /* pp              unconditional jump (pp=position)*/
        case 0x02: return; /* oo oo pp        call subroutine (oo=object pointer)*/
        case 0x03: return; /*                 return from subroutine*/
        case 0x04: return; /* tt              delay (tt=time)*/
        case 0x05: return; /* ff              object disappears when flag set*/
        case 0x06: return; /* ff              object appears when flag set*/
        case 0x07: return; /*                 (unused, freezes game)*/
        case 0x08: return; /* tt tt           display text*/
        case 0x09: return; /* pp              ask yes/no, jump if "no" selected or B pressed*/
        case 0x0A: return; /* pp              jump unless TALKing*/
        case 0x0B: return; /* pp              jump unless CHECKing*/
        case 0x0C: return; /* ii pp           jump unless using PSI (01=telepathy)*/
        case 0x0D: return; /* ii pp           jump unless using item*/
        case 0x0E: return; /*                 (unused, freezes game)*/
        case 0x0F: return; /*                 reset NES*/
        case 0x10: return; /* ff              set flag*/
        case 0x11: return; /* ff              clear flag*/
        case 0x12: return; /* ff pp           jump unless flag set*/
        case 0x13: return; /* cc              decrease counter*/
        case 0x14: return; /* cc              increase counter*/
        case 0x15: return; /* cc              set counter to 0*/
        case 0x16: return; /* cc nn pp        jump if counter less than value*/
        case 0x17: return; /* vv nn           change map variable (1B-1E)*/
        case 0x18: return; /* pp              choose character, jump if B pressed*/
        case 0x19: return; /* cc              select specific character*/
        case 0x1A: return; /* cc pp           jump unless character selected*/
        case 0x1B: return; /* pp              jump if no money added to bank acct since last call*/
        case 0x1C: return; /* pp              input a number, jump if B pressed*/
        case 0x1D: return; /* nn nn           load a number*/
        case 0x1E: return; /* nn nn pp        jump if number less than value*/
        case 0x1F: return; /*                 show money*/
        case 0x20: return; /* pp              choose item from inventory, jump if B pressed*/
        case 0x21: return; /* pp              choose item from closet, jump if B pressed*/
        case 0x22: return; /* ii ii ii ii pp  choose item from list, jump if B pressed*/
        case 0x23: return; /* ii pp           jump unless item in character's inventory*/
        case 0x24: return; /* ii pp           jump unless item in closet*/
        case 0x25: return; /* ii              select specific item*/
        case 0x26: return; /* ii pp           jump unless item selected*/
        case 0x27: return; /* ii pp           jump unless item in any character's inventory*/
        case 0x28: return; /* pp              give money, jump if can't hold any more*/
        case 0x29: return; /* pp              take money, jump if not enough*/
        case 0x2A: return; /* pp              add to bank account, jump if can't hold any more*/
        case 0x2B: return; /* pp              take from bank account, jump if not enough*/
        case 0x2C: return; /* pp              jump if item unsellable*/
        case 0x2D: return; /* pp              add item to inventory, jump if full*/
        case 0x2E: return; /* pp              remove item from inventory, jump if not present*/
        case 0x2F: return; /* pp              add item to closet, jump if full*/
        case 0x30: return; /* pp              remove item from closet, jump if not present*/
        case 0x31: return; /* nn pp           select character's nn'th item (first is 0), jump if empty slot*/
        case 0x32: return; /* nn              multiply number by nn/100*/
        case 0x33: return; /* cc pp           jump if character not present*/
        case 0x35: return; /* pp              jump unless touching object*/
        case 0x37: return; /* tt tt p1 p2     show 2-option menu, jump to p1 if second option selected*/
                           /* jump to p2 if B selected*/
        case 0x38: return; /* pp              jump if no items in inventory*/
        case 0x39: return; /* pp              jump if no items in closet*/
        case 0x3A: return; /* nn pp           select nn'th character in party (first is 0), jump if not present*/
        case 0x3B: return; /* tt              change object type (tt=type) e.g. 26=run away*/
        case 0x3D: return; /* xx xx yy yy     teleport player*/
        case 0x3E: return; /* mm mm           move object (mm=pointer to movement data, after script)*/
        case 0x3F: return; /* oo              signal another object (oo=object number)*/
        case 0x40: return; /* pp              jump unless signaled*/
        case 0x41: return; /*                 teleport to saved game location*/
        case 0x42: return; /* cc pp           add character to party, jump if party full*/
        case 0x43: return; /* cc pp           remove character from party, jump if absent*/
        case 0x44: return; /* gg              start battle (gg=enemy group)*/
        case 0x45: return; /*                 multiply by number of characters*/
        case 0x46: return; /* dd              rocket (dd=direction)*/
        case 0x47: return; /* dd              airplane*/
        case 0x48: return; /* dd              tank*/
        case 0x49: return; /* dd              boat*/
        case 0x4A: return; /*                 train*/
        case 0x4B: return; /* dd              elevator*/
        case 0x4C: return; /* dd              no vehicle*/
        case 0x50: return; /* pp              jump if at less than max HP*/
        case 0x51: return; /* nn              heal HP*/
        case 0x52: return; /* ss pp           jump if character has status*/
        case 0x53: return; /* ss              remove statuses not in ss*/
        case 0x54: return; /* ll pp           jump if character below level*/
        case 0x55: return; /*                 sleep*/
        case 0x56: return; /*                 save game*/
        case 0x57: return; /*                 load character's exp needed for next level*/
        case 0x58: return; /*                 load money*/
        case 0x59: return; /* ss              inflict status on character*/
        case 0x5A: return; /* mm              change background music*/
        case 0x5B: return; /* ss              play sound (1)*/
        case 0x5C: return; /* ss              play sound (2)*/
        case 0x5D: return; /* ss              play sound (3)*/
        case 0x5E: return; /*                 (unused, freezes game)*/
        case 0x5F: return; /*                 teach characters 1 and 2 to Teleport*/
        case 0x60: return; /* pp              jump if at less than max PP*/
        case 0x61: return; /* nn              heal PP*/
        case 0x62: return; /* pp              take weapon, jump if none*/
        case 0x63: return; /* pp              select confiscated weapon, jump if none*/
        case 0x64: return; /*                 live show*/
        case 0x65: return; /* pp              jump unless all 8 melodies learned*/
        case 0x66: return; /*                 register your name*/
        case 0x67: return; /*                 darken palette (Magicant end)*/
        case 0x68: return; /*                 land mine*/
        case 0x69: return; /*                 horiz. shake (EVE?)*/
        case 0x6A: return; /*                 XX crystal*/
        }
    }
}
