#ifndef MELODY_CAMERA_H_
#define MELODY_CAMERA_H_

#include "cngine/cngine.h"
#include "actor.h"

void UpdateCamera(CN_Rect *camera, CN_Rect *map, struct Actor *actor);

#endif
