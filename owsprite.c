#include "owsprite.h"

static const int ROM_BANK_OFFSET = 0x22010;
/* horizontal * vertical tiles */
static const int GFX_PAGE_SIZE = 16 * 8;
/* Start of sprites in CHR */
static const int INITIAL_GFX_PAGE = 48;
static const int GLOBAL_SPRITE_X_OFFSET = 0x10;
static const int GLOBAL_SPRITE_Y_OFFSET = 0x18;

static CN_inline CN_DrawDirection GetDrawDirectionFromPalAndDir(char palAndDir)
{
    char temp = palAndDir >> 4;
    switch(temp){
    case 0x0:
    case 0x1:
        return CN_DRAWDIRECTION_NORMAL;
    case 0x4:
    case 0x5:
        return CN_DRAWDIRECTION_FLIPX;
    case 0x8:
    case 0x9:
        return CN_DRAWDIRECTION_FLIPY;
    case 0xc:
    case 0xd:
        return CN_DRAWDIRECTION_FLIPX | CN_DRAWDIRECTION_FLIPY;
    default: return CN_DRAWDIRECTION_NONE;
    }
}

void DrawSpriteQuad(
    CN_Image **graphics,
    MotheriNESROM *rom,
    SpriteQuadInfo *info,
    int x,
    int y)
{
    int offset = info->pointerEnd*0x100 + info->pointerStart + ROM_BANK_OFFSET;

    DrawSprite(graphics,
               (SpriteData*)(&rom->data.rom.bytes[offset]),
               0,
               info->additionalOffset,
               info->fourPals,
               x,
               y);
    DrawSprite(graphics,
               (SpriteData*)(&rom->data.rom.bytes[offset+sizeof(SpriteData)]),
               0,
               info->additionalOffset,
               info->fourPals,
               x,
               y);
    DrawSprite(graphics,
               (SpriteData*)(&rom->data.rom.bytes[offset+(sizeof(SpriteData)*2)]),
               0,
               info->additionalOffset,
               info->fourPals,
               x,
               y);
    DrawSprite(graphics,
               (SpriteData*)(&rom->data.rom.bytes[offset+(sizeof(SpriteData)*3)]),
               0,
               info->additionalOffset,
               info->fourPals,
               x,
               y);
}

void DrawSprite(
    CN_Image **graphics,
    SpriteData *data,
    int tileset,
    unsigned char additionalOffset,
    unsigned char fourPals,
    int x,
    int y)
{
    const int PAL_OFFSET = 16;
    /* First two bits are character pal*/
    char offsetOfPalIndex = data->palAndRot & 0x03;
    char actualPalOffset;

    /* palNumber decides which two bits to get */
    actualPalOffset = fourPals >> (offsetOfPalIndex * 2) & 0x03;
    
    /* Second two bits are unused? */
    /* Fifth bit is draw behind (or is it supposed to be invisible?)
       Unused for now. */
    CN_bool drawBehind = (data->palAndRot >> 5 ) & 1;
    /* last three? are direction, I think */
    CN_DrawDirection dir = GetDrawDirectionFromPalAndDir(data->palAndRot);;
    CN_Image_DrawMaskedOffsetFlip(
        graphics[
            (tileset + INITIAL_GFX_PAGE) * GFX_PAGE_SIZE +
            + additionalOffset +
            data->tileNum],
        CN_GetScreen(),
        x + data->x - (GLOBAL_SPRITE_X_OFFSET),
        y + data->y - (GLOBAL_SPRITE_Y_OFFSET),
        PAL_OFFSET+(actualPalOffset)*4,
        dir);
}
