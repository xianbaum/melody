#include "player.h"

void InitPlayerActor(struct Actor *actor)
{
    actor->x = 1024;
    actor->y = 0;
    actor->targetX = 1024;
    actor->targetY = 0;
    actor->dir = OBJDIR_DOWN;
    actor->spriteQuadOffset = 0;
    actor->step = 0;
    actor->stepTimer = 0;
}

void HandleInput(struct Actor *actor)
{
    if(actor->x == actor->targetX &&
       actor->y == actor->targetY &&
       actor->dir == actor->targetDir) {

        if(CN_IsKeyDown(CN_KEY_LEFT))
        {
            actor->targetX -= 16;
            actor->targetDir = OBJDIR_LEFT;
        }
        else if(CN_IsKeyDown(CN_KEY_RIGHT))
        {
            actor->targetX += 16;
            actor->targetDir = OBJDIR_RIGHT;
        }

        if(CN_IsKeyDown(CN_KEY_UP))
        {

            actor->targetY -= 16;
            actor->targetDir = OBJDIR_UP;
        }
        else if(CN_IsKeyDown(CN_KEY_DOWN))
        {
            actor->targetY += 16;
            actor->targetDir = OBJDIR_DOWN;
        }
    }
}
