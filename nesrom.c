#include <stdio.h>
#include "nesrom.h"
#include <stdlib.h>

const int MOTHER_INES_ROM_SIZE = 393232;
const int MOTHER_NES_ROM_SIZE = 393216;
const int PPU_TILE_COUNT = 8192;

MotheriNESROM *MotheriNESROM_LoadByPath(char *path)
{
    FILE *file = fopen(path, "r");
    MotheriNESROM *rom;
    if(file == NULL) return NULL;
    rom = malloc(sizeof(MotheriNESROM));
    fread(rom->bytes, sizeof(char), MOTHER_INES_ROM_SIZE, file);
    fclose(file);
    return rom;
}
