#ifndef NESCHR_H_
#define NESCHR_H_

#include "cngine/cngine.h"
#include "nesrom.h"

typedef struct RGB4ColorPal{
    CN_RGB colors[4];
} RGB4ColorPal;

void FreeCHRGraphics(CN_Image **graphics);
void DrawCHRPage(CN_Image **graphics, int offset);
CN_Image **GenerateGFXFromROM(MotheriNESROM *rom);
CN_Image *CHRToCNImage(PPUTile tile);
void DrawTextPage(MotheriNESROM *rom, CN_Image **graphics, int offset);
int CharToGraphicsOffset(unsigned char input);
int TenTenOrDakuten(unsigned char input);
RGB4ColorPal *RGB4ColorPalFromNES(NES4ColorPal *nespal, CN_RGB *nespalrgb);
void RGB4ColorPal_Apply(RGB4ColorPal *rgbpal, CN_RGB *pal, int offset);
void RGB4ColorPal_Draw(RGB4ColorPal *nespal, int x, int y);
void NES4ColorPal_Draw(NES4ColorPal *nespal, CN_RGB *nespalrgb, CN_RGB *pal);
char PreparePalette(
    unsigned char palIndex,
    NES4ColorPal *pals,
    CN_RGB *nespalrgb,
    CN_RGB *pal,
    int start,
    int palCount,
    CN_bool fixFourth);
    
#endif /*NESCHR_H_*/
