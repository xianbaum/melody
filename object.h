#ifndef MELODY_OBJECT_H_
#define MELODY_OBJECT_H_

#include "cngine/cngine.h"
#include "nesrom.h"
#include "map.h"
#include "owsprite.h"

enum ObjectDir {
    OBJDIR_NONE = 0,
    OBJDIR_UP = 1,
    OBJDIR_RIGHT = 2,
    OBJDIR_DOWN = 3,
    OBJDIR_LEFT = 6
};

enum ObjectType {
    OBJTYPE_NONE = 0x00,
    OBJTYPE_UNKNOWN_COLLIDEABLE = 0x01,
    OBJTYPE_UNKNOWN_NOTHING_COLLIDEABLE = 0x02,
    OBJTYPE_UNKNOWN_NOTHING_COLLIDEABLE2 = 0x03,
    OBJTYPE_DOOR = 0x04,
    OBJTYPE_UNKNOWN_NOTHING = 0x05,
    OBJTYPE_UNKNOWN_NOTHING_2 = 0x06,
    OBJTYPE_UNKNOWN_NOTHING_3 = 0x07,
    OBJTYPE_PLAYER_WHAT = 0x08,
    OBJTYPE_PLAYER_CANNOT_INTERACT = 0x09,
    OBJTYPE_PLAYER_CANNOT_INTERACT_2 = 0x0A,
    OBJTYPE_PLAYER_CANNOT_INTERACT_3 = 0x0B,
    OBJTYPE_PLAYER_STANDING_WHAT = 0x0C,
    OBJTYPE_STRAIGHT_UP_LEFT_CAM_FOLLOWS = 0x0D,
    OBJTYPE_STANDING_CANNOT_INTERACT = 0x0E,
    OBJTYPE_STANDING_CAMERA_FOLLOWS = 0x0F,
    OBJTYPE_STANDING = 0x10,
    OBJTYPE_WALKING = 0x11,
    OBJTYPE_WALKING_FASTER = 0x12,
    OBJTYPE_PANICKING_STANDING = 0x13,
    OBJTYPE_STANDING_2 = 0x14,
    OBJTYPE_WALKING_2 = 0x15,
    OBJTYPE_WALKING_FASTER_2 = 0x16,
    OBJTYPE_PANICKING_STANDING_2 = 0x17,
    OBJTYPE_INVISIBLE_CRASHES = 0x18,
    OBJTYPE_SIGNPOST_MAYBE = 0x19,
    OBJTYPE_DOUBLE_SPRITE_NO_COLLISION = 0x1a,
    OBJTYPE_INVISIBLE_CRASHES_2 = 0x1b,
    OBJTYPE_INVISIBLE_CRASHES_3 = 0x1c,
    OBJTYPE_SIGNPOST_MAYBE_2 = 0x1d,
    OBJTYPE_DOUBLE_SPRITE_NO_COLLISION_2 = 0x1e,
    OBJTYPE_INVISIBLE_CRASHES_4 = 0x1f
};

/* This just puts them into one uniform struct> Not all fields are relevant */
struct ParsedObject
{
    char type;
    unsigned short x;
    unsigned short y;
    unsigned short sprite;
    unsigned short sector;
    enum ObjectDir dir;
    char presentItem;
    char presentID;
    char music;
    char targetDir;
    short doorX;
    short doorY;
    unsigned char *script;
};

struct ParsedObject *ParseObjectsForArea(
    MotheriNESROM *rom, int area, MapSector *sectors, int *len);

void DrawParsedObject(
    MotheriNESROM *rom,
    CN_Image **graphics,
    CN_Rect *camera,
    struct ParsedObject *object);

#endif
