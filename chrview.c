#include <stdio.h>
#include <stdlib.h>
#include "cngine/cngine.h"
#include "nesrom.h"
#include "neschr.h"
#include "map.h"
#include "owsprite.h"
#include "actor.h"
#include "player.h"
#include "camera.h"
#include "object.h"
#include <stddef.h>

static void run(MotheriNESROM *rom);
static void loop(MotheriNESROM *rom);
static CN_RGB *pal;
int main(int argc, char **argv)
{
    if(argc == 2) {
        MotheriNESROM *rom = MotheriNESROM_LoadByPath(argv[1]);
        if(rom != NULL) run(rom);
        else printf("Invalid ROM path.\n");
    }
    else printf("Usage: chrview [path of MOTHER rom]\n");
    return 0;
}

static void run(MotheriNESROM *rom)
{
    CN_Options *o = CN_Options_CreateBlank();
    o->width = 426;
    o->height = 240;
    o->windowTitle = "MELODY";
    if (!CN_Check(CN_Init(o))) return;
    CN_LogInfo(("Program started."));
    pal = CN_RGB_CreateBlank();
    loop(rom);
    CN_free(o);
}


static void loop(MotheriNESROM *rom)
{
    int dt = 0, offset = 0, objlen, i;
    CN_Image **gfx = GenerateGFXFromROM(rom);
    int gfx_offset = 0, gfx_offset2 = 0, debugnum = 0;
    CN_RGB *nespal = CN_RGB_ExtractFromImage("palette.pcx");
    MapData *data = CreateMapDataFromROM(rom, nespal);
    CN_RGB *pal = CN_malloc(sizeof(CN_RGB)*256);;
    unsigned char mapArea = 0x01;
    CN_Rect map;
    struct ParsedObject *obj;
    memcpy(pal, nespal, sizeof(CN_RGB)*256);
    CN_RGB_Set(pal);
    SearchForMapArea(data, mapArea, &gfx_offset, &gfx_offset2);
    obj = ParseObjectsForArea(rom, mapArea, data->sectors, &objlen);
    gfx_offset *= 64;
    gfx_offset2 *= 64;
    map.x = gfx_offset;
    map.y = gfx_offset2;
    map.w = map.h= 32000;
    PreparePalette(
        mapArea,
        rom->data.rom.data.prg.interpreted.mapPalettes,
        nespal,
        pal,
        0,
        3,
        CN_true);
    PreparePalette(
        0,
        rom->data.rom.data.prg.interpreted.characterPalettes,
        nespal,
        pal,
        16,
        32,
        CN_false);
    struct Actor *player = CN_malloc(sizeof(struct Actor));
    CN_Rect camera ={.x = map.x, .y = map.y,
                            .w = CN_options->width, .h =CN_options->height};
    InitPlayerActor(player);
    player->x = player->targetX  = map.x;
    player->y = player->targetY = map.y;
    player->x = player->targetX  = 0x2b * 64;
    player->y = player->targetY = (0x4f - 0x20) * 64 - 16;
    while (!CN_ExitCondition()) {
        CN_Backend_Update();
        dt += CN_GetDeltaTime();
        if (CN_IsKeyDown(CN_KEY_ESC)) CN_exit = CN_true;
        if (dt > 1) dt = 1;
        else CN_Sleep(10);
        if (CN_GetFramesSinceLastCall() > 0) {
            CN_Cls();
            if(CN_IsKeyPressed(CN_KEY_P)) {
                CN_LogDebug(("(%d,%d)", gfx_offset/64, gfx_offset2/64));
            }
            else if(CN_IsKeyPressed(CN_KEY_O)) {
                debugnum++;
                CN_LogDebug(("debugnum: %d", debugnum));
            }

            HandleInput(player);
            UpdateCamera(&camera, &map, player);
            DrawMap(gfx, data, &camera, mapArea);
/*            NES4ColorPal_Draw(
                &rom->data.rom.data.prg.interpreted.mapPalettes[palnum],
                nespal, pal);*/
            
            DrawActor(rom, gfx, &camera, player);
            for(i = 0; i < objlen; i++) {
                DrawParsedObject(rom, gfx, &camera, obj+i);
            }                    
            UpdateActor(player, 1);
/*            DrawSpriteQuad(gfx,
                           rom,
                           rom->data.rom.data.prg.interpreted.spriteQuads+debugnum,
                           0,0);*/
            CN_Flip();
            CN_Backend_FinalizeFrame();
        }
    }
    CN_free(player);
    FreeCHRGraphics(gfx);
    CN_RGB_Free(nespal);
} 
