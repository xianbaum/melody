NO_LUA_CFLAGS	= -DCN_NO_LUA

#SDL 1.2
SDL1_2_LINKER_M   = -lSDL
SDL1_2_IMAGE_LINK = -lSDL_image
SDL1_2_MIX_LINKER = -lSDL_mixer -lmikmod -lwinmm -lm
SDL1_2_MW_LINKER  = -lmingw32 -lSDLmain -lSDL -mwindows 
SDL1_2_CFLAGS     = -DCN_SDL1_2 -DCN_NO_MIXER
SDL1_2_LINKER	= $(SDL1_2_LINKER_M) $(SDL1_2_IMAGE_LINK) $(SDL_1_2_MIX_LINKER)

#SDL 2
SDL2_LINKER     = -lSDL2 -lSDL2_image
SDL2_CFLAGS     = -DCN_SDL2 -DCN_NO_MIXER -DCN_NO_LUA

#Allegro 4.4/4.2
ALLEGRO44_LINKER= `pkg-config --cflags --libs allegrogl allegro ` -lm
ALLEGRO44_MW_L  = -lallegro-4.4.2-md -lallegrogl-0.4.4-md
ALLEGRO42_LINKER= -lm -lalleg
LALGIF_LINKER   = -lalgif
DUMB_LINKER 	= -laldmd -ldumbd
JGMOD_LINKER	= -ljgmod
ALLEGRO4_CFLAGS = -DCN_ALLEGRO4
JGMOD_CFLAGS	= -DUSE_JGMOD_
DUMB_CFLAGS    	= -DUSE_DUMB_
ALLEGRO4_SPLINT	= .splintrc

OPTIONS_NEW_GCC = -Wextra -Winit-self -Wuninitialized -Wmissing-declarations
BIN_DIR 	= bin/
CC		= gcc
DEBUG		= -g
CFILES:= $(wildcard *.c) $(wildcard cngine/*.c) $(wildcard cngine/ds/*.c)
OBJ_FILES := $(addprefix obj/,$(subst ,,$(CFILES:.c=.o)))

ifeq ($(OS),Windows_NT)
	SDL_LINKER_MW = -lmingw32 -lSDL2main
else

endif

.PHONY: sdl1_2
sdl1_2: mkdirs sdl1_2_c

sdl1_2_c: OPTIONS += $(OPTIONS_NEW_GCC)
sdl1_2_c: CFLAGS += $(NO_LUA_CFLAGS) $(SDL1_2_CFLAGS)

sdl1_2_c: $(OBJ_FILES)
	$(CC) -o $(BIN_DIR)melody $^ $(SDL1_2_LINKER)


.PHONY: allegro
allegro: mkdirs allegro_c

allegro_c: OPTIONS += $(OPTIONS_NEW_GCC)
allegro_c: CFLAGS += $(NO_LUA_CFLAGS) $(ALLEGRO4_CFLAGS)

allegro_c: $(OBJ_FILES)
	$(CC) -o $(BIN_DIR)melody $^ $(ALLEGRO44_LINKER)

.PHONY: sdl2
sdl2: mkdirs sdl2c

sdl2c: OPTIONS += $(OPTIONS_NEW_GCC)
sdl2c: CFLAGS += $(SDL2_CFLAGS)
sdl2c: $(OBJ_FILES)
	$(CC) -o $(BIN_DIR)melody $^ $(SDL_LINKER_MW) $(SDL2_LINKER)

mkdirs:
	mkdir -p obj/cngine/ds
	mkdir -p bin

obj/%.o: %.c
	$(CC) -c $(OPTIONS) $(CFLAGS) $(INCLUDE) $< $(OBJS) $(DEBUG) $(CFLAGS) -o $@

clean:
	rm -rf obj
	rm -rf bin

check-syntax: sdl2
