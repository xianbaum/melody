#ifndef MELODY_PLAYER_H_
#define MELODY_PLAYER_H_
#include "actor.h"

void InitPlayerActor(struct Actor *actor);
void HandleInput(struct Actor *actor);

#endif
