#ifndef MELODY_OWSPRITE_H_
#define MELODY_OWSPRITE_H_

#include "cngine/cngine.h"
#include "nesrom.h"
#include "object.h"

void DrawOverworldGeneralSprite(
    CN_Image **graphics,
    OverworldSpriteQuad *fourTileWalkData,
    int characterPalOffset,
    int tileset,
    int twoByEightOffset,
    int x,
    int y);

void DrawSpriteQuad(
    CN_Image **graphics,
    MotheriNESROM *rom,
    SpriteQuadInfo *info,
    int x,
    int y);

void DrawSprite(
    CN_Image **graphics,
    SpriteData *data,
    int tileset,
    unsigned char additionalOffset,
    unsigned char fourPals,
    int x,
    int y);

#endif
