#include "neschr.h"

CN_Image *CHRToCNImage(PPUTile tile)
{
    CN_Image *image = CN_Image_Create(8, 8);
    int y = 0;
    for(; y < 8; y++) {
        int x = 0;
        for(; x < 8; x++) {
            int index  = (tile.data[y] & 1)*1 + (tile.data[y+8] & 1)*2;
            tile.data[y] = tile.data[y] >> 1;
            tile.data[y+8] = tile.data[y+8] >> 1;
            CN_Image_PutPixel(image, 7-x, y, index);
        }
    }
    return image;
}

void DrawTextPage(MotheriNESROM *rom, CN_Image **graphics, int offset)
{
    int x = 0, destx, desty;
    for(; x < 16; x++) {
        int y = 0;
        for(; y <= 240/8; y++) {
            destx = x * 8;
            desty = y * 8;
            CN_Image_Draw(
                graphics[CharToGraphicsOffset(
                        rom->bytes[offset+(y*16)+x]
                        )],
                CN_GetScreen(), destx, desty);
        }
    }
}

void DrawCHRPage(CN_Image **graphics, int offset)
{
    int x = 0, destx, desty;
    for(; x < 16; x++) {
        int y = 0;
        for(; y <= 240/8; y++) {
            if(offset+(y*16)+x > 8192) continue;
            destx = x * 8;
            desty = y * 8;
            CN_Image_Draw(
                graphics[offset+(y*16)+x],
                CN_GetScreen(),
                destx, desty);
        }
    }
}

void FreeCHRGraphics(CN_Image **graphics)
{
    int i = 0;
    if(graphics == NULL) return;
    for(; i < 8192; i++) CN_Image_Free(graphics[i]);
    CN_free(graphics);
}

CN_Image **GenerateGFXFromROM(MotheriNESROM *rom)
{
    CN_Image **graphics = CN_malloc(sizeof(CN_Image*)*8192);
    int i = 0;
    for(; i < 8192; i++) {
        graphics[i] = CHRToCNImage(rom->data.rom.data.chr[i]);
    }
    return graphics;
}
static const int TEXT_START = 7936;
int CharToGraphicsOffset(unsigned char input)
{
    /*Below 0x40 also require tenten except for
      0x15 thru 0x1F and 0x35 thru 0x3F which require dakuten.*/
    if(input >= 0x40) return 7936 + input;
    if(input >= 0x01 && input < 0x1F) return TEXT_START + input + 0x90;
    if(input >= 0x1F && input < 0x35) return TEXT_START + input + 0xA0;
    if(input >= 0x35 && input < 0x40) return TEXT_START + input + 0xB0;
    return 7999;
}

int TenTenOrDakuten(unsigned char input)
{
    static const int TENTEN_INDEX = 8191;
    static const int DAKUTEN_INDEX = 8190;
    static const int BLANK_INDEX = 7999;
    if((input >= 0x15 && input <= 0x1F) ||
       (input >= 0x35 && input <= 0x3F)) return TENTEN_INDEX;
    if(input <= 0x40 && input >= 0x01) return DAKUTEN_INDEX;
    return BLANK_INDEX;
}

RGB4ColorPal *RGB4ColorPalFromNES(NES4ColorPal *nespal, CN_RGB *nespalrgb)
{
    RGB4ColorPal *rgbpal = CN_malloc(sizeof(RGB4ColorPal));
    int i; 
    for(i = 0; i < 4; i++) rgbpal->colors[i] = nespalrgb[nespal->colors[i]];
    return rgbpal;
}

void RGB4ColorPal_Apply(RGB4ColorPal *rgbpal, CN_RGB *pal, int offset)
{
    int i = 0;
    for(; i < 4; i++) pal[offset+i] = rgbpal->colors[i];
    CN_RGB_Set(pal);
}

void RGB4ColorPal_Draw(RGB4ColorPal *rgbpal, int x, int y)
{
    const int w = 64;
    CN_Image *image = CN_Image_Create(w,w);
    int i = 0;
    
    for(; i < w*w; i++) {
        CN_Image_PutPixel(
            image, i % w, i / w,
            (i%w >= w/2 ? 1:0) + (i >= w*(w/2) ? 2:0));
    }
    CN_Image_Draw(image, CN_GetScreen(), x, y);
    CN_Image_Free(image);
}

void NES4ColorPal_Draw(NES4ColorPal *nespal, CN_RGB *nespalrgb, CN_RGB *pal)
{
    RGB4ColorPal *rgbpal = RGB4ColorPalFromNES(nespal, nespalrgb);
    RGB4ColorPal_Apply(rgbpal, pal, 0);
    RGB4ColorPal_Draw(rgbpal, 0, 0);
    CN_free(rgbpal);
}

char PreparePalette(
    unsigned char palIndex,
    NES4ColorPal *pals,
    CN_RGB *nespalrgb,
    CN_RGB *pal,
    int start,
    int palCount,
    CN_bool fixFourth)
{
    int i;
    RGB4ColorPal *rgbpal;
    const int WHITE = 0x30;
    const int BLACK = 0x0f;
    for(i = 0; i < palCount; i++) {
        rgbpal = RGB4ColorPalFromNES(
            pals+((palIndex*4)+i), nespalrgb);
        RGB4ColorPal_Apply(rgbpal, pal,start+ i*4);
        CN_free(rgbpal);
    }
    rgbpal = RGB4ColorPalFromNES(pals+((palIndex*4)+3), nespalrgb);
    /*The fourth palette is... odd.*/
    if(fixFourth) {
        rgbpal->colors[0] = nespalrgb[BLACK];
        rgbpal->colors[2] = nespalrgb[WHITE];
        RGB4ColorPal_Apply(rgbpal, pal, start+(i*4));
    }
    CN_free(rgbpal);
}
