#ifndef MELODY_ACTOR_H_
#define MELODY_ACTOR_H_
#include "object.h"
#include "owsprite.h"

struct Actor {
    unsigned short x;
    unsigned short y;
    unsigned short targetX;
    unsigned short targetY;
    unsigned short targetDir;
    enum ObjectDir dir;
    int spriteQuadOffset;
    int step;
    int stepTimer;
};

void DrawActor(
    MotheriNESROM *rom,
    CN_Image **graphics,
    CN_Rect *camera,
    struct Actor *actor);

void UpdateActor(struct Actor *actor, int dt);

#endif
