#ifndef MELODY_MAP_H_
#define MELODY_MAP_H_

#include "nesrom.h"
#include "neschr.h"
#include "cngine/cngine.h"

typedef struct MapData {
    MapTile *map;
    Tile16x16 *tiles16;
    Tile64x64 *tiles64;
    TileSubPalette *subpals;
    MapSector *sectors;
    RGB4ColorPal **pals;
} MapData;

typedef struct Tileset16 {
    CN_Image **graphics;
    Tile16x16 *tiles;
} Tileset16;

typedef struct Tileset64 {
    Tileset16 tileset16;
    Tileset16 secondary16;
    Tile64x64 *tiles64;
} Tileset64;

void DrawTile16x16(CN_Image **graphics, Tile16x16 tile, unsigned char subpal,
                   int x, int y, int gfx_offset);
void DrawTile16x16Page(CN_Image **graphics, Tile16x16 *tiles, int offset,
                       TileSubPalette *subpals, int gfx_offset);
void DrawTile64x64(
    Tile64x64 tile, TileSubPalette subpal,
    Tileset16 tileset1, Tileset16 tileset2, int x, int y);
void DrawTile64x64Page(
    Tile64x64 *tiles64, CN_Image **graphics, Tile16x16 *tiles16, int offset,
    int tilesetnum1, int tilesetnum2, TileSubPalette *subpals);
void DrawTileset(CN_Image **graphics, int gfx_offset);
void DrawMap(CN_Image **graphics, MapData *mapdata, CN_Rect *camera,
             unsigned char paletteIndex);
Tile64x64 *Tileset64OffsetFromNumber(int number, Tile64x64 *tiles64);
#define DEFINE_TILE_FUNCTION(_TYPE_, _NAME_)    \
    _TYPE_ *_NAME_(MotheriNESROM *rom)

MapData *CreateMapDataFromROM(MotheriNESROM *rom, CN_RGB *nesPal);
Tileset16 TilesetFromNumber(int number, CN_Image **graphics, Tile16x16*tiles16);
Tileset16 Tileset16FromNumber(int number, CN_Image **graphics,
                              Tile16x16 *tiles16);
char PrepareAreaBySectorCoordinate(MapSector *sectors, NES4ColorPal *pals, 
                                   CN_RGB *nespalrgb, CN_RGB *pal,int x, int y);
void SearchForMapArea(MapData *mapdata, unsigned char area, int *outx, int *outy);
MapSector *MapSectorFromCoordinate(MapSector*sectors, short x, short y);

static const int TILE_SIZE = 8;
static const int TILE16_SIZE = 16;
static const int TILE64_SIZE = 64;
static const int TILES_PER_TILE16 = 2;
static const int TILE16_PER_TILE64 = 4;
static const int TILES_PER_TILESET = 64;
static const int TOTAL_TILES16 = 4096;
static const int TOTAL_TILES64 = 2048;
static const int MAP_WIDTH = 256;
static const int MAP_HEIGHT = 224;
static const int TOTAL_MAP_TILES = 57334;
static const int SECTOR_WIDTH = 4;
static const int SECTOR_BANK_SIZE = 2;
static const int PAL_OFFSET = 0x2920F;
static const unsigned char ANY_PALETTE_INDEX = 255;



#endif /* #ifndef MELODY_MAP_H_ */
